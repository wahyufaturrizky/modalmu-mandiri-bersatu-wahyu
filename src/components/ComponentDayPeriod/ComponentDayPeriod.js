/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class ComponentDayPeriod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDay: false,
    };
  }

  toggleSelectDay = () => {
    this.setState({selectedDay: !this.state.selectedDay});
  };
  render() {
    const {valueDay} = this.props;
    return (
      <TouchableOpacity style={{
                    width: '14%',
                    marginHorizontal: '1%',
                    marginBottom: RFValue(10),
                    shadowOpacity: 1,
                    elevation: 2,
                    height: undefined,
                    aspectRatio: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    elevation: 2,
                    borderRadius: RFValue(10),
                  }} onPress={this.toggleSelectDay}>
        {this.state.selectedDay ? (
        <View style={{
                        alignItems: 'center',
                        height: '100%',
                        width: '100%',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(74, 157, 219, 0.5)',
                        borderColor: 'rgba(74, 157, 219, 1)',
                        borderWidth: 2,
                        borderRadius: RFValue(10),
                      }}>
          <Text style={{fontSize: RFValue(16), color: "#4A9DDB"}}>{valueDay}</Text>
        </View>
        ) : (
        <View style={{
                      alignItems: 'center',
                      height: '100%',
                      width: '100%',
                      justifyContent: 'center',
                      backgroundColor: 'rgba(157, 163, 180, 0.10)',
                      borderRadius: RFValue(10),
                    }}>
          <Text style={{fontSize: RFValue(16), color:"#828282"}}>{valueDay}</Text>
        </View>
        )}
      </TouchableOpacity>
    );
  }
}
