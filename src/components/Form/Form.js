import React from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import Icon from "react-native-vector-icons/MaterialIcons";
import LinearGradient from 'react-native-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';

export const LabelForm = (props) => {
  return (
    <Text
      style={props.style !== undefined ? [styles.labelStyle, props.style] : styles.labelStyle}
    >
        {props.value}
    </Text>
  );
}

export const TextCenter = (props) => {
  return (
    <View 
      style={props.style !== undefined ? [styles.textCenterStyle, props.style] : styles.textCenterStyle}
    >
      <Text>
        {props.value}
      </Text>
      <TouchableOpacity onPress={props.onPress} disabled={props.disabled ? props.disabled : false}>
        <Text style={props.style !== undefined ? [styles.textForgotPassword, props.style] : styles.textForgotPassword}>{props.forgotPasswordText}</Text>
      </TouchableOpacity>
    </View>
  );
}

export const InputForm = (props) => {
  return (
    <TextInput
      value={props.value}
      style={props.style 
        ? props.editable || props.editable === undefined
        ? [styles.inputStyle, props.style] 
        : [styles.inputStyle, styles.inputdisable, props.style]
        : props.editable 
        ? [styles.inputdisable, styles.inputStyle]
        : styles.inputStyle } autoCapitalize="none"
      placeholder= {props.placeholder}
      onChangeText={props.onChangeText}
      secureTextEntry={props.secureTextEntry}
      keyboardType={props.keyboardType ? props.keyboardType : "default"}
      editable={props.editable !== undefined ? props.editable : true}
    />
  );
}

export const Password = (props) => {
  return (
    <View style={styles.passwordContainer}>
      <TextInput
        value={props.value}
        style={styles.passwordStyle}
        autoCapitalize="none"
        placeholder= {props.placeholder}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureText}
      />
      <TouchableOpacity onPress={props.changeVisibility}>
        <Icon
          style={{paddingHorizontal: 10}}
          size={20}
          name={props.secureText ? "visibility" : "visibility-off"}
        />
      </TouchableOpacity>
    </View>
  );
}

export const TextArea  = (props) => {
  return (
    <TextInput value={props.value} style={props.style ? [styles.inputStyle, props.style] : styles.inputStyle} placeholder= {props.placeholder} onChangeText={props.onChangeText} multiline={true} numberOfLines={props.numberOfLines} />
  )
}

export const SAButton = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} disabled={props.disabled ? props.disabled : false} >
      <View
      style={[
        styles.buttonStyle,
        props.backgroundColor ? {backgroundColor: props.backgroundColor} : {},
        props.style ? props.style : {},
        props.disabled ? styles.disableButton : {}
      ]}>
        <Text style={{fontFamily: "NunitoSans-SemiBold",color: props.fontColor ? props.fontColor : "white", textAlign: "center", fontSize: props.fontSize? props.fontSize : 14}}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  )
}

export const SAGradientButton = (props) => {
  return (
    <TouchableOpacity style={props.style}>
      <LinearGradient 
        colors={['#46BCFF', '#3A7BD5']} 
        style={styles.linearGradient}
      >
        <Text style={styles.buttonText}>
          {props.title}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  )
}

let styles = {
  linearGradient: {
    // flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    borderRadius: RFValue(20),
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  labelStyle: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#BDBDBD",
  },
  textForgotPassword: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#1FB2DC",
  },
  textCenterStyle: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginTop: widthPercentageToDP('4%'),
    marginBottom: widthPercentageToDP('4%'),
  },
  passwordContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 0,
    marginTop: widthPercentageToDP('1%'),
    marginBottom: widthPercentageToDP('2%'),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderColor: "#F9ED33",
    borderWidth: 1
  },
  passwordStyle: {
    flex: 1,
    fontFamily: "NunitoSans-Regular",
    color: "#172344",
    backgroundColor: "white",
    paddingHorizontal: widthPercentageToDP('3%'),
    paddingVertical: widthPercentageToDP('2%'),
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  inputStyle: {
    fontFamily: "NunitoSans-Regular",
    color: "#172344",
    paddingHorizontal: widthPercentageToDP('3%'),
    paddingVertical: widthPercentageToDP('2%'),
    marginTop: widthPercentageToDP('1%'),
    marginBottom: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderColor: "#31BCFD",
    borderWidth: 1
  },
  inputdisable: {
    backgroundColor: "#D3D3D3"
  },
  buttonStyle: {
    justifyContent:"center",
    fontFamily: "NunitoSans-SemiBold",
    padding: widthPercentageToDP('3%'),
    marginVertical: widthPercentageToDP('3%'),
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  disableButton: {
    backgroundColor: "#707070"
  }
}