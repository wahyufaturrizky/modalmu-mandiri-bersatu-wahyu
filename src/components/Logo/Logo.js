import React, { Component } from "react";
import { View, Image } from "react-native";

class Logo extends Component {
  render() {
    return (
      <View>
        <Image
          source={require("../../assets/images/logo-min.png")}
          style={{ width: 70, height: 70, marginLeft: -20 }}
        />
      </View>
    );
  }
}

export default Logo;
