import React, { Component } from "react";
import { View, Image, Text } from "react-native";

class Logo extends Component {
  state = {
    icon: {
      calculator_icon: require(`../../assets/images/home-icon/calculator.png`),
      booking_icon: require(`../../assets/images/home-icon/booking.png`),
      report_icon: require(`../../assets/images/home-icon/report.png`),
      tracking_icon: require(`../../assets/images/home-icon/tracking.png`),
      news_icon: require(`../../assets/images/home-icon/news.png`),
      more_icon: require(`../../assets/images/home-icon/more.png`),
      icon_tujuan_meminjam: require(`../../assets/images/icon_tujuan_meminjam.png`),
      icon_pilihan_paket: require(`../../assets/images/icon_pilihan_paket.png`),
      aktivitas: require(`../../assets/images/aktivitas.png`),
      beranda: require(`../../assets/images/beranda.png`),
      profil: require(`../../assets/images/profil.png`),
      aktivitas_1: require(`../../assets/images/aktivitas_1.png`),
      beranda_1: require(`../../assets/images/beranda_1.png`),
      profil_1: require(`../../assets/images/profil_1.png`),

      home_1: require(`../../assets/images/home_1.png`),
      inbox_1: require(`../../assets/images/inbox_1.png`),
      account_1: require(`../../assets/images/account_1.png`),
      help_1: require(`../../assets/images/help_1.png`),
      pay: require(`../../assets/images/pay.png`),
      pay_1: require(`../../assets/images/pay_1.png`),
      status_appraisal_1: require(`../../assets/images/status_appraisal_1.png`),
      home: require(`../../assets/images/home.png`),
      inbox: require(`../../assets/images/inbox.png`),
      account: require(`../../assets/images/account.png`),
      help: require(`../../assets/images/help.png`),
      status_appraisal: require(`../../assets/images/status_appraisal.png`),
      appraisal: require(`../../assets/images/appraisal.png`),
      pricing_calculator: require(`../../assets/images/pricing_calculator.png`),
      status_report: require(`../../assets/images/status_report.png`),
      tracking: require(`../../assets/images/tracking.png`),
      news: require(`../../assets/images/news.png`),
      more: require(`../../assets/images/more.png`),
      success: require(`../../assets/images/ic_success.png`),
      send: require(`../../assets/images/send.png`),
      calendar: require(`../../assets/images/calendar.png`),
    },
  };
  render() {
    let { name, width, height } = this.props;
    let { icon } = this.state;

    return (
      <View>
        <Image source={icon[name]} style={{ width, height }} />
      </View>
    );
  }
}

export default Logo;
