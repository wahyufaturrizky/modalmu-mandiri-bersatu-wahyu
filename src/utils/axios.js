import axios from "axios";
import Config from "react-native-config";
import store from "../store/configureStore";
import AsyncStorage from '@react-native-community/async-storage';

const select = async (state) => {
  let userToken = await AsyncStorage.getItem("userDetail");
  userToken = JSON.parse(userToken)
  return userToken.token
}

let instance = axios.create({
  baseURL: Config.API_URL,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
  },
});

const listener = async () => {
  let token = await select(store.getState());
  instance.defaults.headers.common["Authorization"] = token;
}

store.subscribe(listener);
export default instance;
