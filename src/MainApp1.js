import React, {Component, useEffect} from "react";
import TrustIcon from "./components/TrustIcon/TrustIcon";
import { Provider } from "react-redux";
import {createStackNavigator, HeaderBackButton} from 'react-navigation-stack';
import { createSwitchNavigator, createAppContainer} from "react-navigation";
import {createBottomTabNavigator} from 'react-navigation-tabs';

import store from "./store/configureStore";
import HomepageScreen from "./screens/Homepage/Homepage";
import LoginScreen from "./screens/Login/Login";
import ProfileScreen from "./screens/Profile/Profile";
import BookingScreen from "./screens/Booking/Booking";
import AppraisalScreen from "./screens/StatusAppraisal/StatusAppraisal";
import LogoutScreen from "./screens/Logout/Logout";
import PricingCalculatorScreen from "./screens/PricingCalculator/PricingCalculator";
import PricingCalculatorResultScreen from "./screens/PricingCalculatorResult/PricingCalculatorResult";
import AppraisalDetailScreen from "./screens/AppraisalDetail/AppraisalDetail";
import UnderConstructionScreen from "./screens/UnderConstruction/UnderConstruction";
import OnBoardingScreen from "./screens/OnBoarding/OnBoarding";
import ReportScreen from './screens/Report/Report';

// const AppraisalNavigator = () => {
//   return (
//     <Stack.Navigator>
//       <Stack.Screen 
//         name = 'Appraisal Status' 
//         component = {AppraisalScreen}
//         options = {{
//           headerTitle: "Status Appraisal",
//           headerLeft: <HeaderBackButton tintColor="#fff" onPress={() => navigation.navigate("Homepage")} />,
//         headerStyle: {
//           backgroundColor: "#005596",
//         },
//         headerTintColor: "#fff",
//         headerTitleStyle: {
//           fontFamily: "NunitoSans-Black",
//         },
//         }}
//       />
//       <Stack.Screen 
//         name = 'Appraisal Status' 
//         component = {AppraisalScreen}
//         options = {{
//           headerTitle: "Status Appraisal",
//           headerLeft: <HeaderBackButton tintColor="#fff" onPress={() => navigation.navigate("Homepage")} />,
//         headerStyle: {
//           backgroundColor: "#005596",
//         },
//         headerTintColor: "#fff",
//         headerTitleStyle: {
//           fontFamily: "NunitoSans-Black",
//         },
//         }}
//       />
//     </Stack.Navigator>
//   )
// }
const AppraisalNavigator = createStackNavigator(
  {
    StatusAppraisal: {
      screen: AppraisalScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Status Appraisal",
        headerLeft: <HeaderBackButton tintColor="#fff" onPress={() => navigation.navigate("Homepage")} />,
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }),
    },
    AppraisalDetail: {
      screen: AppraisalDetailScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Detail Appraisal",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      }),
    },
  },
  {
    initialRouteName: "StatusAppraisal",
  },
);

const HomeStackNavigator = createStackNavigator(
  {
    Homepage: {
      screen: HomepageScreen,
    },
    PricingCalculator: {
      screen: PricingCalculatorScreen,
      navigationOptions: {
        title: "Pricing Calculator",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      },
    },
    PricingCalculatorResult: {
      screen: PricingCalculatorResultScreen,
      navigationOptions: {
        title: "Pricing Result",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      },
    },
    Appraisal: {
      screen: BookingScreen,
      navigationOptions: {
        title: "Book Appraisal",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      },
    },
    StatusAppraisal: {
      screen: AppraisalNavigator,
      navigationOptions: { header: null },
    },
    Report: {
      screen: ReportScreen,
      navigationOptions: {
        title: "Report",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      },
    },
    UnderConstruction: {
      screen: UnderConstructionScreen,
      navigationOptions: {
        title: "Sedang Dalam Pembuatan",
        headerStyle: {
          backgroundColor: "#005596",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "NunitoSans-Black",
        },
      },
    },
    Logout: {
      screen: LogoutScreen,
    },
  },
  {
    initialRouteName: "Homepage",
  },
);

HomeStackNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


const HomeTabNavigator = createBottomTabNavigator(
  {
    Home: HomeStackNavigator,
    Inbox: UnderConstructionScreen,
    Bantuan: UnderConstructionScreen,
    Profil: ProfileScreen,
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        if (routeName === "Home") {
          iconName = `home${focused ? "" : "_1"}`;
        } else if (routeName === "Profil") {
          iconName = `account${focused ? "" : "_1"}`;
        } else if (routeName === "Inbox") {
          iconName = `inbox${focused ? "" : "_1"}`;
        } else if (routeName === "Bantuan") {
          iconName = `help${focused ? "" : "_1"}`;
        }

        return <TrustIcon name={iconName} width={25} height={25} />;
      },
    }),
    lazy: false,
    tabBarOptions: {
      activeTintColor: "#005596",
      activeBackgroundColor: "white",
      inactiveTintColor: "#BDBEC0",
      style: {
        elevation: 10,
        borderTopColor: "#DCE7EF",
        backgroundColor: "white",
        height: 60,
      },
    },
  },
);

const FrontpageNavigator = 
createAppContainer(
  createSwitchNavigator(
    {
      OnBoarding: OnBoardingScreen,
      Login: LoginScreen,
      Homepage: HomeTabNavigator,
    },
    {
      initialRouteName: "Login",
    },
  )
);

const Navigation = () => (
  <Provider store={store}>
    <FrontpageNavigator />
  </Provider>
);
export default Navigation;
