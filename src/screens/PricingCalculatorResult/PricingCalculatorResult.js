import React, { Component } from "react";
import { View, Text, ScrollView } from "react-native";
import { axios, widthPercentageToDP, heightPercentageToDP } from "../../utils";
import { formatNumber } from "accounting";

import { SAButton } from "../../components/Form";

class PricingCalculatorResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carDetail: {},
      result: false,
      carKM: null,
    };
    this.goToAppraisal = this.goToAppraisal.bind(this);
  }

  componentDidMount() {
    let { route } = this.props;
    const { carBrandId, carType, carSpec, carYear } = route.params
    axios
      .get(`/car/price-year/${carBrandId}/${carType}/${carSpec}/${carYear}`)
      .then(result => {
        this.setState({
          carDetail: result.data,
          result: true,
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  goToAppraisal() {
    this.props.navigation.navigate("Appraisal");
  }

  render() {
    const { carDetail } = this.state;
    const { carBrand, carType, carSpec, carPrice } = carDetail;
    return (
      <View style={styles.container}>
        <ScrollView>
          {!this.state.result ? (
            <View>
              <Text>Loading...</Text>
            </View>
          ) : (
            <View
              style={[
                styles.container,
                { paddingHorizontal: widthPercentageToDP("4%"), paddingBottom: widthPercentageToDP("6%") },
              ]}
            >
              <Text style={styles.title}>Perkiraan Harga</Text>

              <View style={{ zIndex: 1000 }}>
                <View style={styles.brandBox}>
                  <Text style={styles.brandName}>{carBrand.brand}</Text>
                </View>
              </View>

              <View style={styles.specTypeBox}>
                <Text
                  style={{
                    color: "#005596",
                    fontFamily: "NunitoSans-Black",
                    textAlign: "center",
                    fontSize: 22,
                  }}
                >
                  {carType.carType}
                </Text>
                <Text
                  style={{
                    fontFamily: "NunitoSans-SemiBold",
                    textAlign: "center",
                    color: "#172344",
                    marginTop: 5,
                  }}
                >
                  {carSpec.carSpec}
                </Text>
              </View>

              <View style={styles.priceForecasts}>
                <Text style={{ color: "white" }}>Rp. {formatNumber(carPrice.priceMin, 0, ".", ",")}</Text>
                <View
                  style={{
                    backgroundColor: "white",
                    width: widthPercentageToDP("15%"),
                    height: 2,
                    marginTop: heightPercentageToDP("1.5%"),
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                  }}
                />
                <Text style={{ color: "white" }}>Rp. {formatNumber(carPrice.priceMax, 0, ".", ",")}</Text>
              </View>

              <View style={[styles.priceForecasts, styles.infoArea]}>
                <View style={styles.boxInfo}>
                  <Text style={styles.infoTitle}>Estimasi Kilometer</Text>
                  <Text style={styles.infoContent}>{`< ${formatNumber(carPrice.kmMax, 0, ".", ",")}`}</Text>
                </View>

                <View style={styles.boxInfo}>
                  <Text style={styles.infoTitle}>Estimasi Warna</Text>
                  <Text style={styles.infoContent}>Hitam/Putih</Text>
                </View>

                <View style={[styles.boxInfo, { borderRightWidth: 0 }]}>
                  <Text style={styles.infoTitle}>Estimasi Tahun</Text>
                  <Text style={styles.infoContent}>{carPrice.year}</Text>
                </View>
              </View>

              <View style={[styles.priceForecasts, styles.notesBox]}>
                <View style={{ alignItems: "center", marginBottom: heightPercentageToDP("3%") }}>
                  <Text style={{ fontSize: 20, color: "#005596", fontWeight: "600" }}>Keterangan</Text>
                  <View style={styles.borderTitle} />
                </View>

                <Text
                  style={{
                    marginBottom: heightPercentageToDP("2.5%"),
                    fontFamily: "NunitoSans-SemiBold",
                    textAlign: "center",
                    fontSize: 16,
                  }}
                >
                  Harga yang diberikan merupakan{" "}
                  <Text style={{ fontFamily: "NunitoSans-Black", color: "#FB3434" }}>estimasi awal</Text> yang dpat
                  ditawarkan Toyota Trust dan{" "}
                  <Text style={{ fontFamily: "NunitoSans-Black", color: "#FB3434" }}>
                    bukan merupakan harga final setelah proses appraisal
                  </Text>
                </Text>
              </View>

              <SAButton
                onPress={this.goToAppraisal}
                backgroundColor="#FB8B34"
                title="Booking Appraisal"
                style={{
                  width: widthPercentageToDP("92%"),
                  fontSize: 18,
                }}
              />
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    height: "100%",
    color: "#064C77",
  },
  title: {
    fontFamily: "NunitoSans-Black",
    color: "#172344",
    fontSize: 20,
    marginVertical: heightPercentageToDP("4%"),
    letterSpacing: 1,
  },
  brandBox: {
    backgroundColor: "#005596",
    paddingVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("5%"),
    borderRadius: 5,
  },
  brandName: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: 18,
    color: "white",
  },
  specTypeBox: {
    alignItems: "center",
    marginTop: -widthPercentageToDP("5%"),
    paddingTop: widthPercentageToDP("8%"),
    paddingBottom: widthPercentageToDP("4%"),
    paddingHorizontal: widthPercentageToDP("4%"),
    borderColor: "#DCE7EF",
    borderWidth: 2,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  priceForecasts: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: widthPercentageToDP("92%"),
    marginVertical: heightPercentageToDP("2%"),
    padding: widthPercentageToDP("3%"),
    backgroundColor: "#005596",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  infoArea: {
    justifyContent: "flex-start",
    padding: 0,
    backgroundColor: "white",
    borderColor: "#DCE7EF",
    borderWidth: 2,
  },
  boxInfo: {
    width: "33.33%",
    marginVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("2%"),
    alignItems: "center",
    borderColor: "#DCE7EF",
    borderRightWidth: 2,
  },
  infoTitle: {
    fontFamily: "NunitoSans-SemiBold",
    textAlign: "center",
    color: "#172344",
    fontSize: 13,
    marginBottom: 5,
  },
  infoContent: {
    fontFamily: "NunitoSans-SemiBold",
    textAlign: "center",
    color: "#005596",
    fontSize: 16,
  },
  notesBox: {
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "#DCE7EF",
    borderWidth: 2,
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  borderTitle: {
    width: widthPercentageToDP("15%"),
    marginTop: heightPercentageToDP("1%"),
    height: 2,
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
};

export default PricingCalculatorResult;
