import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text } from "react-native";

class UnderConstruction extends Component {
  constructor(props) {
    super(props);
    this.navigationWillFocusListener = props.navigation.addListener("willFocus", () => {
      this.checkSalesProfile();
    });
  }

  checkSalesProfile() {
    const { SalesProfile } = this.props;
    if (!SalesProfile.onRequest) {
      if (!SalesProfile.id) {
        this.props.navigation.navigate("Profil");
      }
    }
  }

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Sedang Dalam Pembuatan</Text>
      </View>
    );
  }
}

const mapStateToProps = ({ SalesProfile }) => ({ SalesProfile });

export default connect(mapStateToProps)(UnderConstruction);
