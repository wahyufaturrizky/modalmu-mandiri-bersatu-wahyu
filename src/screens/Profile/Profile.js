import React, { Component } from "react";
import axios from "axios";
import pickObj from "lodash/pick";
import Config from "react-native-config";
import ImagePicker from "react-native-image-picker";
import DatePicker from "react-native-datepicker";
import Permissions from "react-native-permissions";
import moment from "moment";
import { connect } from "react-redux";
import { Header } from "react-native-elements";
import { Text, View, ScrollView, Picker, Image, TouchableOpacity } from "react-native";
import { bindActionCreators } from "redux";
import Toast from "react-native-easy-toast";
import RNPicker from "../../components/RNPickerSearch";

import { LabelForm, SAButton, InputForm } from "../../components/Form";
import { setSalesProfile } from "../../store/actions";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";

class Profilepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onRequestSimpan: false,
      name: "",
      branch: "",
      nrp: "",
      phone: "",
      whatsapp: "",
      dateOfBirth: "1990-01-01",
      bankName: "",
      bankBeneficiary: "",
      bankAccNo: "",
      noNPWP: "",
      npwp: "",
      NPWP: "",
      previewNPWP: "",
      ktp: "",
      KTP: "",
      previewKTP: "",
      cameraPermission: "",
      photoPermission: "",
      uploadProgress: 0,
      supervisorId: null,
      branchHeadId: null,
      isUpdated: true,
      supervisors: [],
      branch_heads: [],
      selected_supervisor: "",
      selected_branchHead: "",
      error_message: '',
      empty_field: false,
    };
    this._onChangeText = this._onChangeText.bind(this);
    this.selectImageToUpload = this.selectImageToUpload.bind(this);
    this.selectedValue = this._selectedValue.bind(this)
  }

  toastFillProfile() {
    if (!this.props.SalesProfile.id) {
      this.refs.toast.show("Silahkan isi User Profile terlebih dahulu");
    }
  }

  _onChangeText(val, field) {
    this.setState({
      [field]: val,
    });
  }

  _selectedValue(index, item, field) {
    this.setState({ 
      [field+'Id']: +item.id,
      [`selected_${field}`]: item.name,
     });
  }

  _onPressSimpan() {
    const { Auth } = this.props;
    this.setState({
      onRequestSimpan: true,
      isUpdated: true,
      empty_field: false
    });
    let formData = new FormData();
    let obj = pickObj(this.state, [
      "supervisorId",
      "branchHeadId",
      "phone",
      "whatsapp",
      "dateOfBirth",
      "bankName",
      "bankBeneficiary",
      "bankAccNo",
      "noNPWP",
    ])
    let empty_field = false
    let error_message = null
    for (let key in obj) {
      if((this.state[key] == null || this.state[key] == "") ) {
        empty_field = true
        error_message = `${key} wajib diisi`
        break;
      }
      formData.append(key, this.state[key]);
    }
    formData.append("KTP", this.state.KTP);
    formData.append("NPWP", this.state.NPWP);
    formData.append('isUpdated', true)

    if(!empty_field) {
      axios({
        method: "post",
        url: `${Config.API_URL}/user/${Auth.user.id}/sales-profile`,
        timeout: 60 * 3 * 1000,
        data: formData,
        headers: {
          authorization: Auth.token,
          accept: "application/json",
        },
        onUploadProgress: progressEvent => {
          this.setState({ uploadProgress: Math.round((progressEvent.loaded * 100) / progressEvent.total) });
        },
      })
        .then(result => {
          result.data.salesProfile.supervisorId = +result.data.salesProfile.supervisorId
          result.data.salesProfile.branchHeadId = +result.data.salesProfile.branchHeadId
          this.setState({ 
            ...result.data.salesProfile });
          this.props.setSalesProfile(result.data.salesProfile);
          this.refs.toast.show("Data berhasil di update", 1000, () => {
            this.props.navigation.navigate("Homepage");
          });
        })
        .catch(err => {
          console.log(err.message)
          this.refs.toast.show("Proses update data gagal");
        })
        .finally(() => {
          this.setState({
            onRequestSimpan: false,
            uploadProgress: 0,
            previewKTP: "",
            previewNPWP: "",
          });
        });
    } else {
      this.refs.toast.show(error_message);
    }
  }

  selectImageToUpload(field) {
    const options = {
      title: `Pilih ${field}`,
      storageOptions: {
        path: "images",
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("Pick image cancelled");
      } else if (response.error) {
        console.log("Error pick image", response.error);
      } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: response.type,
            name: field,
          },
          [`preview${field}`]: "data:image/jpeg;base64," + response.data,
        });
      }
    })
  }

  cancelUploadPict = pict => {
    this.setState({
      [pict]: "",
      [`preview${pict}`]: "",
    });
  };

  _requestPermission = type => {
    Permissions.request(type).then(response => {
      this.setState({
        [`${type}Permission`]: response,
      });
    });
  };

  componentDidMount() {
  
    this.toastFillProfile()
    let findSPV = this.props.SalesProfile.supervisors.find(spv => spv.id === this.props.SalesProfile.supervisorId)
    let findBH = this.props.SalesProfile.branch_heads.find(bh => bh.id === this.props.SalesProfile.branchHeadId)
    Permissions.checkMultiple(["camera", "photo"]).then(response => {
      if (response.camera === "undetermined") {
        this._requestPermission("camera");
      }
      if (response.photo === "undetermined") {
        this._requestPermission("photo");
      }
      this.setState({
        cameraPermission: response.camera,
        photoPermission: response.photo,
        ...this.props.SalesProfile,
        selected_supervisor: findSPV ? findSPV.name: "",
        selected_branchHead: findBH ? findBH.name: ""
      });
    });
  }

  render() {
    const { ktp, npwp, onRequestSimpan } = this.state;
    return (
      <View style={styles.containerScreen}>
        <Toast position="top" positionValue={70} opacity={0.75} ref="toast" />
        <Header
          backgroundColor="#005596"
          outerContainerStyles={{ height: 50, elevation: 5 }}
          leftComponent={<Text style={styles.headerText}>Profil</Text>}
          rightComponent={
            <TouchableOpacity onPress={() => this._onPressSimpan()}>
              <Text style={styles.saveBtn}>Simpan</Text>
            </TouchableOpacity>
          }
        />

        <ScrollView style={styles.ScrollViewStyle}>
          <View style={styles.profileBox}>
            <View style={{ marginBottom: 15 }}>
              <Text style={styles.title}>Data Diri</Text>
              <View style={styles.borderTitle} />
            </View>

            <LabelForm value="Name Sales" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Masukkan Nama Anda"
              value={this.state.name}
              style={styles.formStyle}
              editable={false}
            />

            <LabelForm value="Cabang" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              value={this.state.branch}
              style={styles.formStyle}
              editable={false}
            />

            <LabelForm value="Supervisor" style={{ color: "#172344", marginTop: 10 }} />
            <RNPicker
              dataSource={this.state.supervisors}
              pickerTitle= {"List Supervisor"}
              selectedLabel={this.state.selected_supervisor}
              placeHolderLabel={"Nama Supervisor"}
              selectedValue={this.selectedValue}
              field={"supervisor"}
            />
            <LabelForm value="Kepala Cabang" style={{ color: "#172344", marginTop: 10 }} />
            <RNPicker
              dataSource={this.state.branch_heads}
              pickerTitle= {"List Kepala Cabang"}
              selectedLabel={this.state.selected_branchHead}
              placeHolderLabel={"Nama Kepala Cabang"}
              selectedValue={this.selectedValue}
              field={"branchHead"}
            />

            <LabelForm value="NRP" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Masukkan NRP Anda"
              value={this.state.nrp}
              style={styles.formStyle}
              editable={false}
            />

            <LabelForm value="No. Handphone Aktif" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Masukkan Nomor Handphone Anda"
              value={this.state.phone}
              onChangeText={val => this._onChangeText(val, "phone")}
              style={styles.formStyle}
            />

            <LabelForm value="No. WhatsApp" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Masukkan Nomor WhatsApp Anda"
              value={this.state.whatsapp}
              onChangeText={val => this._onChangeText(val, "whatsapp")}
              style={styles.formStyle}
            />

            <LabelForm value="Tanggal Lahir" style={{ color: "#172344", marginTop: 10 }} />
            <View style={styles.dateTimeStyle}>
              <DatePicker
                date={this.state.dateOfBirth}
                style={{ width: "100%" }}
                iconSource={require(`../../assets/images/calendar.png`)}
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD"
                maxDate="2005-12-30"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                onDateChange={date => this.setState({ dateOfBirth: date })}
                customStyles={{
                  dateInput: {
                    backgroundColor: "white",
                    borderWidth: 0,
                  },
                  placeholderText: {
                    color: "#CAD9E4",
                  },
                  dateText: {
                    color: "#172344",
                    fontSize: 14,
                  },
                }}
              />
            </View>
          </View>

          <View style={styles.profileBox}>
            <View style={{ marginBottom: 15 }}>
              <Text style={styles.title}>Data Rekening</Text>
              <View style={styles.borderTitle} />
            </View>

            <LabelForm value="Bank" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Nama Bank"
              value={this.state.bankName}
              onChangeText={val => this._onChangeText(val, "bankName")}
              style={styles.formStyle}
            />

            <LabelForm value="Nama Penerima" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Nama Rekening Penerima"
              value={this.state.bankBeneficiary}
              onChangeText={val => this._onChangeText(val, "bankBeneficiary")}
              style={styles.formStyle}
            />

            <LabelForm value="Nomor Rekening" style={{ color: "#172344", marginTop: 10 }} />
            <InputForm
              placeholder="Masukkan Nomor Rekening Anda"
              value={this.state.bankAccNo}
              onChangeText={val => this._onChangeText(val, "bankAccNo")}
              style={styles.formStyle}
            />
          </View>

          <View style={styles.profileBox}>
            <View style={{ marginBottom: 15 }}>
              <Text style={styles.title}>Dokumen</Text>
              <View style={styles.borderTitle} />
            </View>

            <View>
              <LabelForm value="Nomor NPWP" style={{ color: "#172344", marginTop: 10 }} />
              <InputForm
                placeholder="Nomor NPWP"
                value={this.state.noNPWP}
                onChangeText={val => this._onChangeText(val, "noNPWP")}
                style={styles.formStyle}
              />

              <LabelForm value="KTP (opsional)" style={{ color: "#172344", marginTop: 10 }} />
              {!!ktp ? (
                <View style={styles.imageDefault}>
                  <Image
                    source={{ uri: `${Config.API_DOMAIN}/uploads/${ktp}` }}
                    style={styles.previewSize}
                    borderRadius={5}
                  />
                </View>
              ) : null}
              {this.state.previewKTP !== "" ? (
                <View>
                  <View style={styles.imagePreview}>
                    <Text
                      style={{
                        fontFamily: "NunitoSans-SemiBold",
                        textAlign: "center",
                        color: "#005596",
                        marginBottom: 10,
                      }}
                    >
                      Image to Upload
                    </Text>
                    <Image
                      source={{ uri: this.state.previewKTP }}
                      style={styles.previewSize}
                      resizeMode="cover"
                      borderRadius={5}
                    />
                  </View>
                  <SAButton
                    backgroundColor="#FB3434"
                    title="Cancel Upload"
                    onPress={() => this.cancelUploadPict("KTP")}
                  />
                </View>
              ) : null}
              <SAButton title="Pilih KTP" onPress={() => this.selectImageToUpload("KTP")} />
            </View>

            <View>
              <LabelForm value="NPWP (optional)" style={{ color: "#172344", marginTop: 10 }} />
              {!!npwp ? (
                <View style={styles.imageDefault}>
                  <Image
                    source={{ uri: `${Config.API_DOMAIN}/uploads/${npwp}` }}
                    style={styles.previewSize}
                    borderRadius={5}
                  />
                </View>
              ) : null}
              {this.state.previewNPWP !== "" ? (
                <View>
                  <View style={styles.imagePreview}>
                    <Text
                      style={{
                        fontFamily: "NunitoSans-SemiBold",
                        textAlign: "center",
                        color: "#005596",
                        marginBottom: 10,
                      }}
                    >
                      Image to Upload
                    </Text>
                    <Image source={{ uri: this.state.previewNPWP }} style={styles.previewSize} borderRadius={5} />
                  </View>
                  <SAButton
                    backgroundColor="#FB3434"
                    title="Cancel Upload"
                    onPress={() => this.cancelUploadPict("NPWP")}
                  />
                </View>
              ) : null}
              <SAButton title="Pilih NPWP" onPress={() => this.selectImageToUpload("NPWP")} />
            </View>

            <View>
              {this.state.uploadProgress > 0 ? <Text>Upload Progress : {this.state.uploadProgress}%</Text> : null}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  containerScreen: {
    backgroundColor: "white",
    height: "100%",
  },
  ScrollViewStyle: {
    paddingHorizontal: widthPercentageToDP("4%"),
    paddingBottom: 50,
  },
  headerText: {
    fontFamily: "NunitoSans-Black",
    color: "white",
    fontSize: 20,
    fontWeight: "500",
  },
  saveBtn: {
    fontFamily: "NunitoSans-Black",
    color: "white",
    fontSize: 18,
    fontWeight: "500",
  },
  profileBox: {
    marginVertical: widthPercentageToDP("4%"),
  },
  title: {
    fontFamily: "NunitoSans-SemiBold",
    fontSize: 24,
    color: "#172344",
  },
  borderTitle: {
    width: widthPercentageToDP("15%"),
    marginTop: heightPercentageToDP("1%"),
    height: 2,
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  formStyle: {
    borderWidth: 1,
    fontSize: 16,
    borderColor: "#DCE7EF",
    marginTop: widthPercentageToDP("2%"),
  },
  borderStyle: {
    marginTop: heightPercentageToDP("1%"),
    marginBottom: heightPercentageToDP("2%"),
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  input: {
    height: heightPercentageToDP("6%"),
    padding: 10,
    color: "#172344",
  },
  dateTimeStyle: {
    marginVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("3%"),
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  imagePreview: {
    marginTop: 10,
  },
  imageDefault: {
    marginVertical: 10,
  },
  previewSize: { width: "100%", height: 200 },
  cancelButton: {
    color: "white",
    backgroundColor: "#BC3E3E",
    marginVertical: 10,
    width: "95%",
    height: 45,
  },
};

const mapStateToProps = ({ Auth, SalesProfile }) => ({ Auth, SalesProfile });
const mapDispatchToProps = dispatch => bindActionCreators({ setSalesProfile }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profilepage);
