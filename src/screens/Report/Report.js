import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import RNPickerSelect from "react-native-picker-select";
import Config from "react-native-config";
import { connect } from "react-redux";

import { widthPercentageToDP, heightPercentageToDP, axios } from '../../utils/index';

class Report extends Component {
	constructor(props) {
		super(props);
		this.state = {
			month: "",
			periods:[],
			result:{}
		 };
	}

	componentDidMount = async() => {
		try {
			let { data } = await axios.get(`${Config.API_URL}/sales-report/month`);
			let periods = await data.periods.map(period => {
				period.value = period.id,
				period.label = period.period
				period.key = period.id
				return period
			});

			this.setState({
				periods
			})
		} catch (e) {
			console.log(e)
		}
	}

	onSelectPeriod = async (id) => {
		try {
			await this.setState({ month: id })
			this.fetchResult(id)
		} catch (e) {
			console.log(e)
		}
	}

	fetchResult = (id) => {
		axios
			.get(`${Config.API_URL}/sales-report/${id}/result`)
			.then (({ data }) => {
				this.setState({
					result: data.result
				})
			} )
	}

	render() {
		const { periods, month, result } = this.state;
		return (
			<View style={styles.container}>
				<View>
					<Text style={styles.periodTitle}>Periode</Text>
					<View style={styles.PeriodPicker}>
					<RNPickerSelect
						value={month}
						items={periods}
						onValueChange={v => this.onSelectPeriod(v)}
					/>
					</View>
				</View>
				<View style={styles.menuContainer}>
						<View style={styles.menu}>
							<Text style={styles.menuTitle}>Cek Harga</Text>
							<Text style={styles.Total}>{result.cekHarga ? result.cekHarga : 0}</Text>
						</View>
						<View style={styles.menu}>
							<Text style={styles.menuTitle}>Appraisal</Text>
							<Text style={styles.Total}>{result.appraisal ? result.appraisal : 0}</Text>
						</View>
						<View style={styles.menu}>
							<Text style={styles.menuTitle}>Deal</Text>
							<Text style={styles.Total}>{result.deal ? result.deal : 0}</Text>
						</View>
						<View style={styles.menu}>
							<Text style={styles.menuTitle}>Poin Podium</Text>
							<Text style={styles.Total}>{result.poin ? result.poin : 0}</Text>
						</View>
					</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: widthPercentageToDP('4%'),
		paddingVertical: widthPercentageToDP('4%'),
		backgroundColor: '#fff'
	},
	periodTitle: {
		color: '#172537',
		fontSize: widthPercentageToDP('4.3%'),
		marginBottom: widthPercentageToDP('2.5%')
	},
	PeriodPicker: {
		borderWidth: 1.5,
		borderColor: '#DCE7EF',
		borderRadius: 5,
		marginBottom: widthPercentageToDP('0.1%'),
	},
	menuContainer: {
		flexWrap: 'wrap',
		flexDirection: 'row',
		paddingTop: widthPercentageToDP('6%'),
		justifyContent: 'space-between'
	},
	menu: {
		width: widthPercentageToDP('44%'),
		paddingVertical: widthPercentageToDP('4%'),
		borderRadius: 5,
		borderColor: '#DCE7EF',
		borderWidth: 1.5,
		backgroundColor: 'white',
		elevation:4,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: "grey",
		shadowOpacity: 0.7,
		shadowRadius: 18,
		borderRadius: 5,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: widthPercentageToDP('4.5%')
	},
	menuTitle: {
		color: '#172537',
		fontSize: widthPercentageToDP('4.5%'),
	},
	Total: {
		color: '#FB8B34',
		fontSize: widthPercentageToDP('9%'),
		fontWeight: 'bold'
	}
})

const mapStateToProps = ({ Auth }) => ({ Auth });

export default connect(mapStateToProps)(Report);