import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActivityIndicator, View, Text, AsyncStorage } from "react-native";

import { authLogout } from "../../store/actions";

class LogoutScreen extends Component {
  state = {
    onProgress: true,
  };
  static navigationOptions = () => ({
    title: "Logout",
  });

  componentDidMount() {
    this.props.authLogout();
  }

  componentDidUpdate(prevProps) {
    console.log(prevProps.Auth.isLogout, "login")
    if (!prevProps.Auth.isLogout && this.props.Auth.isLogout) {
      AsyncStorage.setItem("userDetail", JSON.stringify({ ...this.props.Auth, token: null })).finally(res => {
        setTimeout(() => {
          this.props.navigation.navigate("Login");
        }, 500);
      });
    }
  }

  render() {
    return (
      <View>
        <ActivityIndicator animating={this.state.onProgress} size="large" color="#0000ff" />
      </View>
    );
  }
}

const mapStateToProps = ({ Auth }) => ({ Auth });
const mapDispatchToProps = dispatch => bindActionCreators({ authLogout }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LogoutScreen);
