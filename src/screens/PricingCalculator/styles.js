import {widthPercentageToDP, heightPercentageToDP} from "../../utils";

export default {
  container: {
    flex: 1,
    height: heightPercentageToDP("100%"),
    backgroundColor: "#FFFFFF",
    padding: widthPercentageToDP("4%"),
  },
  formLabel:{
    color: '#005596',
  },
  input: {
    height: heightPercentageToDP("6%"),
    padding: 10,
    color: "#005596",
  },
  buttonContainer: {
    backgroundColor: "#005596",
    paddingVertical: 15,
  },
  buttonText: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "700",
  },
  borderStyle: {
    marginTop: heightPercentageToDP('1%'),
    marginBottom: heightPercentageToDP('2%'),
    borderWidth: 1,
    borderColor: "#DCE7EF",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  buttonArea:{
    flexDirection: "row",
    justifyContent: "space-between",
  },
  containerScreen:  {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    color: "#005596",
  },
  ScrollViewStyle: {
    paddingBottom: 50,
  },
  borderTitle: {
    width: widthPercentageToDP('15%'),
    marginTop: heightPercentageToDP('1%'),
    height: 2,
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  }
};
