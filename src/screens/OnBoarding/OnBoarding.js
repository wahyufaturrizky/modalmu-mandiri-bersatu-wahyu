import React, { Component } from 'react';
import { Image, Text } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import {SAButton} from "../../components/Form";
import OnBoarding from "react-native-onboarding-swiper";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { onBoardState, authLocalJWT, getSalesProfile } from "../../store/actions";

const Done = (props) => (
  <SAButton
    title={'FINISH'}
    fontColor="#fff"
    style={{
      fontFamily: "NunitoSans-Black",
      fontSize: 20,
      backgroundColor: "#4A9DDB",
    }}
    {...props}
  />
);

const Skip = (props) => (
  <SAButton
    title={'SKIP'}
    fontColor="#fff"
    style={{
      fontFamily: "NunitoSans-Black",
      fontSize: 20,
      backgroundColor: "#4A9DDB",
    }}
    {...props}
  />
);

const Next = (props) => (
  <SAButton
    title={'NEXT'}
    fontColor="#fff"
    style={{
      fontFamily: "NunitoSans-Black",
      fontSize: 20,
      backgroundColor: "#4A9DDB",
    }}
    {...props}
  />
);

class OnBoardingScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      isInitiate: false
    }
  }
  async componentWillMount(){
    const isOnBoardFinished = await AsyncStorage.getItem("onBoardFinished")
    let userToken = await AsyncStorage.getItem("userDetail");
    userToken = await JSON.parse(userToken)
    if(userToken !== null){
      if(userToken.token) {
        await this.props.authLocalJWT(userToken);
        await this.props.getSalesProfile(userToken);
        await this.props.navigation.navigate('Mainpage')
      } else {
        await this.props.navigation.navigate('Login')
      }
    } else if(isOnBoardFinished){
      await this.props.navigation.navigate('Login')
    }

    this.setState({
      isInitiate: true
    })
  }

  onFinish() {
    AsyncStorage.setItem("onBoardFinished", "true")
      .then(res => {
        this.props.navigation.navigate("Login");
      })
      .catch(err => {
        console.log(err);
      });
  }

  render () {
    const {isInitiate} = this.state
    if(!this.state.isInitiate) return null;
    return (
      
      <OnBoarding
        skipToPage={4}
        DoneButtonComponent={Done}
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        onDone={() => this.onFinish()}
        bottomBarHighlight={false}
        controlStatusBar={false}
        titleStyles={{fontFamily: "NunitoSans-SemiBold", fontSize: 20, color: "#005596"}}
        subTitleStyles={{fontFamily: "NunitoSans-Regular", color: "#172344", fontSize: 14}}
        containerStyles={{backgroundColor:"#FFF"}}
        controlStatusBar={false}
        pages={[
          {
            backgroundColor: "#4A9DDB",
            image: <Image source={require('../../assets/images/image6.png')} style={{width:150, height: 150}} />,
            title: 'SECURITY',
            subtitle: <Text style={styles.customTitle}>Login <Text style={styles.highlight}>menggunakan email dan password yang telah Anda daftarkan</Text> melalui <Text style={styles.highlight}>Sistem Kami</Text> yang sangat terjaga <Text style={styles.highlight}>kerahasiannya</Text></Text>
          },{
            backgroundColor: "#4A9DDB",
            image: <Image source={require('../../assets/images/image7.png')} style={{width:204.17, height: 132}}  />,
            title: 'HELPFUL',
            subtitle: <Text style={styles.customTitle}>Platform kami tersedia untuk <Text style={styles.highlight}>semua wilayah di Indonesia</Text> dan kami menyambut setiap pertanyaan yang Anda miliki melalui<Text style={styles.highlight}> saluran pertanyaan kami 24-7</Text></Text>,
          },{
            backgroundColor: "#4A9DDB",
            image: <Image source={require('../../assets/images/image8.png')} style={{width:208.89, height: 150}}  />,
            title: 'RAPID',
            subtitle: <Text style={styles.customTitle}><Text style={styles.highlight}>Menunggu itu merepotkan, </Text>kami menyediakan aplikasi <Text style={styles.highlight}>pinjaman cepat dan pencairan cepat, </Text>semuanya hanya dalam waktu <Text style={styles.highlight}>sekitar 5 menit!</Text></Text>,
          },
        ]}
      />
    )
  }
}

const styles= {
  customTitle:{
    textAlign: "center",
    fontFamily: "NunitoSans-Regular",
    color: "#172344",
    fontSize: 15},
  highlight: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#4A9DDB"
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ onBoardState, authLocalJWT, getSalesProfile }, dispatch);

export default connect(null, mapDispatchToProps)(OnBoardingScreen);