import React, { Component } from "react";
import axios from "axios";
import Config from "react-native-config";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import { Text, View, Image, StyleSheet, TouchableOpacity, Linking, Platform, ScrollView , Dimensions} from "react-native";
import Carousel from 'react-native-banner-carousel';
import { Header } from "react-native-elements";
import Slider from 'react-native-slider';

import Button from "../../components/Button/Button";
import TrustIcon from "../../components/TrustIcon/TrustIcon";
import { version as appVersion } from "../../../package.json";
import { widthPercentageToDP, heightPercentageToDP, versionCompare } from "../../utils";
import { Value } from "react-native-reanimated";
import RNPicker from "../../components/RNPickerSearch";
import { LabelForm, SAButton, InputForm, SAGradientButton } from "../../components/Form";
import NumberFormat from 'react-number-format';
import {RFValue} from 'react-native-responsive-fontsize';
import ComponentMonthPeriod from '../../components/ComponentMonthPeriod/ComponentMonthPeriod';
import ComponentDayPeriod from '../../components/ComponentDayPeriod/ComponentDayPeriod';
const BannerWidth = Dimensions.get('window').width;
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalUpdate: false,
      modalLogout: false,
      sliderImages: [],
      value_peminjaman: null,
      value_hari: null,
      value_bulan: null,
      selectedMonth: false,
      selectedDay: false,
    };
    this.navigationWillFocusListener = props.navigation.addListener("willFocus", () => {
      this.checkAppVersion();
      this.checkSalesProfile();
    });
    this.renderPage = this.renderPage.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { SalesProfile } = this.props;
    if (prevProps.SalesProfile.onRequest) {
      if (!SalesProfile.onRequest) {
        if (!SalesProfile.isUpdated) {
          this.props.navigation.navigate("Profil");
        }
      }
    }
  }

  componentDidMount() {
    axios
      .get(`${Config.API_URL}/banner`)
      .then((result) => {
        this.setState({
          sliderImages: result.data.banner
        })
      })
  }

  onPressButton(value) {
    this.props.navigation.navigate(value);
  }

  checkSalesProfile() {
    const { SalesProfile } = this.props;
    if (!SalesProfile.onRequest) {
      if (!SalesProfile.isUpdated) {
        this.props.navigation.navigate("Profil");
      }
    }
  }

  checkAppVersion() {
    axios
      .get(`${Config.API_URL}/com.trustsalesapp`)
      .then(result => {
        const { latestVersion } = result.data;
        if (versionCompare(appVersion, latestVersion) < 0) {
          this.setState({
            modalUpdate: true,
          });
        }
      })
      .catch(error => {
        console.log("error can't get version");
      });
  }

  _onPressLogoutBtn = () => {
    this.setState({
      modalLogout: true,
    });
  };

  openAppLink = () => {
    if (Platform.OS === "android") {
      Linking.openURL("market://details?id=com.trustsalesapp");
    } else if (Platform.OS === "ios") {
      Linking.openURL("https://itunes.apple.com/us/app/auto-trust/id1453700704?mt=8");
    } else {
      console.log("Platform not supported");
    }
  };

  onPressModalLogoutYes() {
    this.setState({ modalLogout: false }, () => {
      this.props.navigation.navigate("Logout");
    });
  }

  onPressModalLogoutNo() {
    this.setState({
      modalLogout: !this.state.modalLogout,
    });
  }

  renderPage(image, index) {
    return (
      <View key={index}>
        <Image style={styles.bannerImage} source={{uri: image}}/>
      </View>
    )
  }

  handleChange = (value, name) => {
    console.log("Yang diketik di", name, " =", value)
    this.setState({
      [name]: value,
    })
  }

  render() {
    const { sliderImages } = this.state;
    return (
      <View style={styles.container}>
        <View style={{position: 'relative'}}>
          <Header
            // borderWidth={2}
            backgroundColor="#fff"
            containerStyle={{height: heightPercentageToDP("30%")}}
            backgroundImageStyle={{resizeMode: "stretch", width: windowWidth}}
            backgroundImage={require("../../assets/images/header_03.png")}
            leftComponent={
              // [START LOGO HEADER]
              // <Image
              //   style={{ width: widthPercentageToDP("15%"), height: widthPercentageToDP("15%"), flex: 1, marginBottom: heightPercentageToDP("15%") }}
              //   resizeMode="contain"
              //   source={require("../../assets/images/modalmu_logo.png")}
              // />
              // [END LOGO HEADER]

              <TouchableOpacity onPress={this._onPressLogoutBtn} style={styles.modalmuHeader}>
                <Text style={styles.logoutText}>Modalmu</Text>
              </TouchableOpacity>
            }
            rightComponent={
              <TouchableOpacity onPress={this._onPressLogoutBtn} style={styles.logoutBtn}>
                <Text style={styles.logoutText}>Logout</Text>
              </TouchableOpacity>
            }
          />
          <Modal isVisible={this.state.modalLogout}>
            <View style={styles.modalLogout}>
              <Text style={styles.modalLogoutTitle}>Logout</Text>
              <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                <Text style={styles.modalLogoutDesc}>Apakah anda yakin ingin logout?</Text>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-around", width: "100%" }}>
                <Button onPress={() => this.onPressModalLogoutNo()} text="Tidak" />
                <Button
                  onPress={() => this.onPressModalLogoutYes()}
                  text="Ya"
                  buttonStyle={{ backgroundColor: "#FB8B34" }}
                />
              </View>
            </View>
          </Modal>
          <Modal isVisible={this.state.modalUpdate}>
            <View
              style={{
                backgroundColor: "white",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 4,
                borderColor: "rgba(0, 0, 0, 0.1)",
                marginHorizontal: 10,
              }}
            >
              <View style={{ padding: 10 }}>
                <View style={{ alignItems: "center" }}>
                  <Text style={{ fontWeight: "600" }}>Update Applikasi Anda</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ textAlign: "justify" }}>
                    Halo Bapak/Ibu wiraniaga AUTO2000! Aplikasi yg Bapak/Ibu gunakan merupakan versi lama. Mohon update ke
                    versi baru aplikasi Bapak/Ibu dengan menekan tombol dibawah ini.
                  </Text>
                  <View style={{ alignItems: "center", marginTop: 20 }}>
                    <Button
                      onPress={() => this.openAppLink()}
                      text="Update"
                      buttonStyle={{ backgroundColor: "#0057A0" }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Modal>
  
          <View style={{position: 'absolute', top: heightPercentageToDP('12%')}}>
            <ScrollView>
              {/* <View style={styles.banner}>
              {sliderImages.length > 0 ? (
                <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={BannerWidth}
                >
                  {sliderImages.map((image, idx) => this.renderPage(image, idx))}
                  <Image source={require("../../assets/images/banner_01.png")} style={styles.bannerImage} />
                  <Image source={require("../../assets/images/banner_02.png")} style={styles.bannerImage} />
                  <Image source={require("../../assets/images/banner_03.png")} style={styles.bannerImage} />
                </Carousel>
              ) : (
                <Image source={require("../../assets/images/banner_03.png")} style={styles.bannerImage} />
              )}
              </View> */}
    
              <View style={styles.menu}>
                <View style={styles.cardBox}>
                  <Text Text style={styles.menuTitleJumlahPeminjaman}>Jumlah pinjaman Rp</Text>
                  <NumberFormat
                    value={this.state.value_peminjaman}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp '}
                    renderText={value => (
                      <Text style={styles.menuTitleJumlahPeminjamanInitial}>{value ? value : <Text>Rp 400,000</Text>}</Text>
                    )}
                  />
    
                  <Slider
                    step={200000}
                    style={{width: 250, height: 40}}
                    minimumValue={400000}
                    maximumValue={5000000}
                    minimumTrackTintColor="#4A9DDB"
                    maximumTrackTintColor="rgba(157, 163, 180, 0.10)"
                    value={400000}
                    thumbStyle={{width: 24, height: 24, backgroundColor: "#fff", borderColor: "#4A6DDB", borderWidth: 2, borderRadius: 20}}
                    trackStyle={{ height: 16, borderRadius: 6 }}
                    onValueChange={value => this.handleChange(value , "value_peminjaman")}
                  />
    
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("10%"),
                      justifyContent: 'space-between',
                    }}
                  >
                    <Text style={styles.styleIndicator}>Rp 0</Text>
                    <Text style={styles.styleIndicator}>Rp 5000000</Text>
                  </View>
    
                  {/* [START Select Periode Bulan] */}
                  {/* <View
                        style={{width: '100%', width: '80%', alignItems: 'center'}}>
                        <LabelForm value="Periode Tersedia (Bulan)" style={{ color: "#BDBDBD", marginTop: RFValue(12) }} />
                        <View
                          style={{
                            alignItems: 'center',
                            width: '100%',
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                          }}>
                          {tables.map((table, index) => (
                            <Table
                              name={table.name}
                              sitCapacity={parseInt(table.sit_capacity)}
                            />
                          ))}
                          <ComponentMonthPeriod valueMonth="1" />
                          <ComponentMonthPeriod valueMonth="2" />
                          <ComponentMonthPeriod valueMonth="3" />
                          <ComponentMonthPeriod valueMonth="4" />
                          <ComponentMonthPeriod valueMonth="5" />
                          <ComponentMonthPeriod valueMonth="6" />
                        </View>
                      </View> */}
                      {/* [END Select Periode Bulan] */}
    
                  {/* [START] Periode Bulan */}
                  <Text Text style={styles.menuTitleJumlahPeminjaman}>Periode Tersedia</Text>
                  <NumberFormat
                    value={this.state.value_bulan}
                    displayType={'text'}
                    thousandSeparator={true}
                    renderText={value => (
                      <Text style={styles.menuTitleJumlahPeminjamanInitial}>{value ? value + " Bulan" : <Text>1 Bulan</Text>}</Text>
                    )}
                  />
    
                  <Slider
                    step={1}
                    style={{width: 250, height: 40}}
                    minimumValue={1}
                    maximumValue={6}
                    minimumTrackTintColor="#4A9DDB"
                    maximumTrackTintColor="rgba(157, 163, 180, 0.10)"
                    value={1}
                    thumbStyle={{width: 24, height: 24, backgroundColor: "#fff", borderColor: "#4A6DDB", borderWidth: 2, borderRadius: 20}}
                    trackStyle={{ height: 16, borderRadius: 6 }}
                    onValueChange={value => this.handleChange(value , "value_bulan")}
                  />
    
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("10%"),
                      justifyContent: 'space-between',
                    }}
                  >
                    <Text style={styles.styleIndicator}>1 Bulan</Text>
                    <Text style={styles.styleIndicator}>6 Bulan</Text>
                  </View>
                  {/* [END] Periode Bulan */}
    
                  {/* <Text Text style={styles.menuTitleJumlahPeminjaman}>Periode Tersedia (Hari)</Text>
                  <NumberFormat
                    value={this.state.value_hari}
                    displayType={'text'}
                    thousandSeparator={true}
                    renderText={value => (
                      <Text style={styles.menuTitleJumlahPeminjamanInitial}>{value ? value + " Hari" : <Text>1 Hari</Text>}</Text>
                    )}
                  />
    
                  <Slider
                    step={1}
                    style={{width: 250, height: 40}}
                    minimumValue={1}
                    maximumValue={30}
                    minimumTrackTintColor="#4A9DDB"
                    maximumTrackTintColor="rgba(157, 163, 180, 0.10)"
                    value={1}
                    thumbStyle={{width: 24, height: 24, backgroundColor: "#fff", borderColor: "#4A6DDB", borderWidth: 2, borderRadius: 20}}
                    trackStyle={{ height: 16, borderRadius: 6 }}
                    onValueChange={value => this.handleChange(value , "value_hari")}
                  />
    
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("10%"),
                      justifyContent: 'space-between',
                    }}
                  >
                    <Text>1 Hari</Text>
                    <Text>30 Hari</Text>
                  </View> */}
                  
                  {/* [START TUJUAN MEMINJAM] */}
                  {/* <LabelForm value="Tujuan Meminjam" style={{ color: "#BDBDBD", marginTop: 10 }} />
                  <RNPicker 
                    dataSource={this.state.supervisors} 
                    pickerTitle={"Tujuan Meminjam"}
                    selectedLabel={this.state.selected_supervisor} 
                    placeHolderLabel={"Tujuan Meminjam"}
                    selectedValue={this.selectedValue} 
                    field={"supervisor"} /> */}
                    {/* [END TUJUAN MEMINJAM] */}
                  
                  {/* START JUMLAH PEMBAYARAN PERTAMA */}
                  {/* <LabelForm 
                    style={{
                      marginTop: heightPercentageToDP("2%"),
                    }}
                    value="Jumlah Pembayaran Pertama" />
                  <InputForm
                    style={{width: widthPercentageToDP("80%")}}
                    editable={true} 
                    placeholder="Nilai" 
                    value={this.state.nrp} 
                    onChangeText={this.onChangeNRP} /> */}
                    {/* END JUMLAH PEMBAYARAN PERTAMA */}

                  {/* [START TUJUAN MEMINJAM] */}
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("10%"),
                      marginTop: RFValue(24),
                      justifyContent: 'space-between',
                    }}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}
                      >
                        <TrustIcon name="icon_tujuan_meminjam" width={20} height={20} />
                        <Text style={styles.styleLabelTujuanMeminjam}>Tujuan Meminjam</Text>
                      </View>
                    </View>
                    <View>
                      <TouchableOpacity>
                        <Text style={styles.styleButtonTujuanMeminjam}>Konsumtif ></Text>
                      </TouchableOpacity>
                    </View>

                  </View>
                  {/* [END TUJUAN MEMINJAM] */}

                  {/* [START PILIHAN PAKET] */}
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingHorizontal: widthPercentageToDP("10%"),
                      marginTop: RFValue(24),
                      justifyContent: 'space-between',
                    }}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}
                      >
                        <TrustIcon name="icon_pilihan_paket" width={20} height={20} />
                        <Text style={styles.styleLabelPilihanPaket}>Pilihan Paket</Text>
                      </View>
                    </View>
                    <View>
                      <TouchableOpacity>
                        <Text style={styles.styleButtonTujuanPilihanPaket}>Premium ></Text>
                      </TouchableOpacity>
                    </View>

                  </View>
                  {/* [END PILIHAN PAKET] */}
    
                  <SAGradientButton 
                    title="Ajukan Sekarang" 
                    style={{marginTop: heightPercentageToDP("2%")}}
                    // onPress={()=>this.onPressButton("SignUp")}
                  />
    
                </View>
              </View>
    
              {/* <View style={styles.menuBox}>
                
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("PricingCalculator")}>
                    <View style={styles.box}>
                      <TrustIcon name="calculator_icon" width={36} height={47} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>Cek Harga</Text>
                </View>
    
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("Appraisal")}>
                    <View style={styles.box}>
                      <TrustIcon name="booking_icon" width={40} height={47} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>Booking</Text>
                </View>
    
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("StatusAppraisal")}>
                    <View style={styles.box}>
                      <TrustIcon name="tracking_icon" width={49} height={39} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>Tracking</Text>
                </View>
    
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("Report")}>
                    <View style={styles.box}>
                      <TrustIcon name="report_icon" width={47} height={47} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>Report</Text>
                </View>
    
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                    <View style={styles.box}>
                      <TrustIcon name="news_icon" width={47} height={43} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>News</Text>
                </View>
    
                <View style={styles.menu}>
                  <TouchableOpacity onPress={() => this.onPressButton("UnderConstruction")}>
                    <View style={styles.box}>
                      <TrustIcon name="more_icon" width={47} height={13} />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.menuTitle}>More</Text>
                </View>
              </View> */}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "white",
    paddingBottom: widthPercentageToDP("4%"),
  },
  banner: {
    margin: widthPercentageToDP("4%"),
  },
  bannerImage: {
    width: widthPercentageToDP("92%"),
    height: widthPercentageToDP("50%"),
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  menuBox: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: widthPercentageToDP("4%"),
  },
  menu: {
    margin: widthPercentageToDP("4%"),
    alignItems: "center",
  },
  menuTitle: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#005596",
  },
  menuTitleJumlahPeminjaman: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#666666",
    fontWeight: "300",
    fontSize: RFValue(18),
    marginTop: RFValue(12)
  },
  menuTitlePeriodeHari: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#666666",
    fontWeight: "300",
    fontSize: RFValue(18),
    marginTop: RFValue(12)
  },
  menuTitleJumlahPeminjamanInitial: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#FF9D2E",
    fontWeight: "bold",
    fontSize: RFValue(35),
  },
  box: {
    width: widthPercentageToDP("22,66%"),
    height: widthPercentageToDP("22,66%"),
    paddingVertical: widthPercentageToDP("4%"),
    borderColor: "#DCE7EF",
    borderWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  cardBox: {
    width: widthPercentageToDP("90%"),
    height: widthPercentageToDP("115%"),
    paddingVertical: widthPercentageToDP("2%"),
    paddingHorizontal: widthPercentageToDP("1%"),
    // borderColor: "#DCE7EF",
    // borderWidth: 4,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: widthPercentageToDP('0%'),
      height: heightPercentageToDP('8%')
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 12,
    alignItems: "center",
    backgroundColor: 'white'
  },
  modalLogout: {
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
    marginHorizontal: 10,
    padding: 20,
  },
  modalLogoutTitle: {
    color: "#005596",
    fontSize: RFValue(20),
    fontFamily: "Nunito Sans",
  },
  modalLogoutDesc: {
    fontFamily: "Nunito Sans",
  },
  logoutText: {
    fontSize: RFValue(16),
    color: "white",
  },
  logoutBtn: {
    padding: widthPercentageToDP("3%"),
    marginBottom: heightPercentageToDP("15%")
  },
  modalmuHeader: {
    padding: widthPercentageToDP("1%"),
    marginBottom: heightPercentageToDP("15%")
  },
  styleIndicator: {
    color: '#9C9C9C',
    fontSize: RFValue(14),
  },
  styleLabelTujuanMeminjam: {
    marginLeft: widthPercentageToDP('2%'),
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleButtonTujuanMeminjam: {
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleLabelPilihanPaket: {
    marginLeft: widthPercentageToDP('2%'),
    color: '#666666',
    fontSize: RFValue(14),
  },
  styleButtonTujuanPilihanPaket: {
    color: '#45B7FB',
    fontSize: RFValue(14),
  },
});

const mapStateToProps = ({ Auth, SalesProfile }) => ({
  Auth,
  SalesProfile,
});

export default connect(mapStateToProps)(Homepage);
