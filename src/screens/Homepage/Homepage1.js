import React, { Component } from "react";
import axios from "axios";
import Config from "react-native-config";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import { Text, View, Image, StyleSheet, TouchableOpacity, Linking } from "react-native";
import { Button } from "react-native-elements";

import TrustIcon from "../../components/TrustIcon/TrustIcon";
import { version as appVersion } from "../../../package.json";
import { versionCompare } from "../../utils";
import {widthPercentageToDP, heightPercentageToDP} from "../../utils";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalUpdate: false,
    };
    this.navigationWillFocusListener = props.navigation.addListener("willFocus", () => {
      this.checkAppVersion();
      this.checkSalesProfile();
    });
  }

  componentDidUpdate(prevProps) {
    const { SalesProfile } = this.props;
    if (prevProps.SalesProfile.onRequest) {
      if (!SalesProfile.onRequest) {
        if (!SalesProfile.id) {
          this.props.navigation.navigate("Profil");
        }
      }
    }
  }

  componentWillUnmount() {
    this.navigationWillFocusListener.remove();
  }

  onPressButton(value) {
    this.props.navigation.navigate(value);
  }

  checkSalesProfile() {
    const { SalesProfile } = this.props;
    if (!SalesProfile.onRequest) {
      if (!SalesProfile.id) {
        this.props.navigation.navigate("Profil");
      }
    }
  }

  checkAppVersion() {
    axios
      .get(`${Config.API_URL}/com.trustsalesapp`)
      .then(result => {
        const { latestVersion } = result.data;
        if (versionCompare(appVersion, latestVersion) < 0) {
          this.setState({
            modalUpdate: true,
          });
        }
      })
      .catch(error => {
        console.log("error can't get version");
      });
  }

  render() {
    return (
      <View style={styles.container}>

          <Modal isVisible={this.state.modalUpdate}>
            <View
              style={{
                backgroundColor: "white",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 4,
                borderColor: "rgba(0, 0, 0, 0.1)",
                marginHorizontal: 10,
              }}
            >
              <View style={{ padding: 10 }}>
                <View style={{ alignItems: "center" }}>
                  <Text style={{ fontWeight: "600" }}>Update Applikasi Anda</Text>
                </View>
                <View style={{ marginTop: 20 }}>
                  <Text style={{ textAlign: "justify" }}>
                    Halo Bapak/Ibu wiraniaga AUTO2000! Aplikasi yg Bapak/Ibu gunakan merupakan versi lama. Mohon update ke
                    versi baru aplikasi Bapak/Ibu dengan menekan tombol dibawah ini.
                  </Text>
                  <View style={{ alignItems: "center", marginTop: 20 }}>
                    <Button
                      onPress={() => Linking.openURL("market://details?id=com.trustsalesapp")}
                      title="Update"
                      backgroundColor="#0057A0"
                      borderRadius={5}
                    />
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <View style={styles.banner}>
            <Image source={require("../../assets/images/banner_podium.png")} style={styles.bannerImage} />
          </View>

          <View style={styles.menuBox}>
              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("PricingCalculator")}>
                  <View style={styles.box}>
                      <TrustIcon name="calculator_icon" width={36} height={47} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>Cek Harga</Text>
              </View>

              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("Appraisal")}>
                  <View style={styles.box}>
                      <TrustIcon name="booking_icon" width={40} height={47} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>Booking</Text>
              </View>

              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("StatusAppraisal")}>
                  <View style={styles.box}>
                      <TrustIcon name="tracking_icon" width={49} height={39} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>Tracking</Text>
              </View>

              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("Report")}>
                  <View style={styles.box}>
                      <TrustIcon name="report_icon" width={47} height={47} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>Report</Text>
              </View>

              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("Appraisal")}>
                  <View style={styles.box}>
                      <TrustIcon name="news_icon" width={47} height={43} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>News</Text>
              </View>

              <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.onPressButton("Report")}>
                  <View style={styles.box}>
                      <TrustIcon name="more_icon" width={47} height={13} />
                  </View>
                </TouchableOpacity>
                <Text style={styles.menuTitle}>More</Text>
              </View>

          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    backgroundColor: "white",
  },
  bannerImage: {
    width: widthPercentageToDP('92%'),
    height: widthPercentageToDP('50%'),
    margin: widthPercentageToDP("4%"),
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  menuBox: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: widthPercentageToDP("4%"),
  },
  menu: {
    margin: widthPercentageToDP("4%"),
    alignItems: "center",
  },
  menuTitle: {
    fontFamily: "NunitoSans-SemiBold",
    color: "#005596",
},
  box: {
    width: widthPercentageToDP("22,66%"),
    height: widthPercentageToDP("22,66%"),
    paddingVertical: widthPercentageToDP("4%"),
    borderColor: "#DCE7EF",
    borderWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: "center",
    justifyContent: "center",
  }
});

const mapStateToProps = ({ Auth, SalesProfile }) => ({
  Auth,
  SalesProfile,
});

export default connect(mapStateToProps)(Homepage);
