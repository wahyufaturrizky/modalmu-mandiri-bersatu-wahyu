import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, Image, ScrollView } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Toast from "react-native-easy-toast";

import { doAuth, authLocalJWT, getSalesProfile } from "../../store/actions";
import BackgroundImage from "../../components/BackgroundImage/BackgroundImage";
import { LabelForm, InputForm, Password, SAButton, TextCenter } from "../../components/Form/Form";
import { widthPercentageToDP } from "../../utils";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      // nrp: "97654",
      // password: "97654",
      loginProgress: false,
      secureText: true,
    };
    this.onPressLogin = this.onPressLogin.bind(this);
    this.onChangeNRP = this.onChangeNRP.bind(this);
    this.onChangeInputPass = this.onChangeInputPass.bind(this);
    this.onChangeVisibility = this.onChangeVisibility.bind(this);
  }

  async componentDidMount() {
    try {
      let userDetail = await AsyncStorage.getItem("userDetail");
      let onBoarding = await AsyncStorage.getItem("onBoardFinished");
      userDetail = await JSON.parse(userDetail)
      if (userDetail) {
        if (userDetail.token){
          try {
            if (userDetail.user) {
              this.setState({
                nrp: userDetail.user.nrp,
                password: userDetail.user.nrp,
              });
            }
            if (userDetail.token) {
              this.props.authLocalJWT(userDetail);
            } else {
              console.log("something wrong", userDetail);
            }
          } catch (error) {
            console.log("error", error);
          }
          this.props.navigation.navigate("Mainpage");
        }
      } else {
        if (onBoarding != "true") {
          this.props.navigation.navigate("OnBoarding");
        }
      }
    } catch (err) {
      console.log(err);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.Auth.isAuthenticated !== this.props.Auth.isAuthenticated) {
      let auth = this.props.Auth
      auth.password = auth.nrp
      AsyncStorage.setItem("userDetail", JSON.stringify(auth))
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log(err);
        });
      this.props.getSalesProfile(this.props.Auth);
      this.props.navigation.navigate("Mainpage");
    }
    if (!prevProps.Auth.error && this.props.Auth.error) {
      this.refs.toast.show("NRP atau Password salah");
    }
  }

  onPressButton(value) {
    this.props.navigation.navigate(value);
  }

  onPressLogin() {
    let user ={
      nrp: this.state.nrp,
      password: this.state.password
    }
    this.props.doAuth(user);
  }

  onChangeNRP(val) {
    this.setState({
      nrp: val.toString(),
    });
  }

  onChangeInputPass(val) {
    this.setState({
      password: val,
    });
  }

  onChangeVisibility() {
    this.setState({
      secureText: !this.state.secureText,
    });
  }

  render() {
    const { Auth } = this.props;
    return (
      <BackgroundImage>
        <Toast position="top" positionValue={10} opacity={0.75} ref="toast" />
          <View style={{ flex: 1, justifyContent: "center"}}>
            <ScrollView>
              <View style={styles.formContainer}>
                <Image
                  source={require("../../assets/images/modalmu_logo.png")}
                  style={{ width: 80, height: 80, alignSelf: "center" }}
                />
    
                <TextCenter 
                  value="Daftar akun" 
                />
    
                <LabelForm 
                  value="Nama" 
                />
                <InputForm 
                  editable={true} 
                  placeholder="Nama Lengkap" 
                  value={this.state.nrp} 
                  onChangeText={this.onChangeNRP}
                />
    
                <LabelForm 
                  value="Email" 
                />
                <InputForm 
                  editable={true} 
                  placeholder="Email" 
                  value={this.state.nrp} 
                  onChangeText={this.onChangeNRP}
                />
    
                <LabelForm 
                  value="Alamat" 
                />
                <InputForm 
                  editable={true} 
                  placeholder="Alamat" 
                  value={this.state.nrp} 
                  onChangeText={this.onChangeNRP}
                />
    
                <LabelForm 
                  value="No Hp" 
                />
                <InputForm 
                  editable={true} 
                  placeholder="No Hp" 
                  value={this.state.nrp} 
                  onChangeText={this.onChangeNRP}
                />
    
                <LabelForm 
                  value="New password" 
                />
                <Password
                  placeholder="New password"
                  value={this.state.password}
                  secureText={this.state.secureText}
                  changeVisibility={this.onChangeVisibility}
                  onChangeText={this.onChangeInputPass}
                />
    
                <LabelForm 
                  value="Confirm password" 
                />
                <Password
                  placeholder="Confirm password"
                  value={this.state.password}
                  secureText={this.state.secureText}
                  changeVisibility={this.onChangeVisibility}
                  onChangeText={this.onChangeInputPass}
                />
    
                {/* <Button color="#FFF" title="Masuk" onPress={this.onPressLogin}/> */}
    
                <SAButton 
                  backgroundColor={"#4A9DDB"} 
                  title="Submit" 
                  onPress={() => this.onPressButton("CodeOTP")}
                />
    
              </View>
            </ScrollView>
          </View>
        <View style={{ height: 30 }} />
      </BackgroundImage>
    );
  }
}

let styles = {
  formContainer: {
    // alignSelf: "center",
    paddingHorizontal: widthPercentageToDP("7%"),
    paddingVertical: widthPercentageToDP("5%"),
  },
};

const mapStateToProps = ({ Auth }) => ({
  Auth,
});

const mapDispatchToProps = dispatch => bindActionCreators({ doAuth, authLocalJWT, getSalesProfile }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
