import { combineReducers } from "redux";
import Auth from "./auth";
import SalesProfile from "./salesProfile";
import SendBird from "./sendbird.js";

export default combineReducers({
  Auth,
  SalesProfile,
  SendBird,
});
