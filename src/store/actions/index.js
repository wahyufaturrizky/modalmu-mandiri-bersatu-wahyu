import axios from "axios";
import Config from "react-native-config";

const doAuth = reqBody => async dispatch => {
  dispatch(authOnRequest());
  try {
    const request = await axios.post(Config.API_URL + "/user/login", reqBody);
    return dispatch(authRequestSuccess(request.data));
  } catch (err) {
    console.log("error ->", err);
    return dispatch(authRequestFailed(err));
  }
};

const onBoardState = reqBody => async dispatch => {
  dispatch(authOnRequest());
  try {
    return dispatch(onBoardVisited());
  } catch (err) {
    console.log("error ->", err);
  }
};

const onBoardVisited = () => {
  return { type: "ONBOARD_VISITED" };
}


const authLocalJWT = loginData => async dispatch => {
  dispatch(authOnRequest());
  try {
    if (loginData.token === undefined) {
      throw new Error("No token in local storage");
    }
    return dispatch(authRequestSuccess(loginData));
  } catch (err) {
    return dispatch(authRequestFailed(err));
  }
};

const authLogout = () => ({ type: "AUTH_LOGOUT" });

const authOnRequest = () => {
  return { type: "AUTH_REQUEST" };
};

const authRequestFailed = err => ({
  type: "AUTH_REQUEST_FAILED",
  payload: err,
});

const authRequestSuccess = payload => ({
  type: "AUTH_REQUEST_SUCCESS",
  payload,
});

const getSalesProfile = payload => async dispatch => {
  dispatch(reqSalesProfile());
  try {
    const request = await axios.get(`${Config.API_URL}/user/${payload.user.id}/sales-profile`, {
      headers: { Authorization: payload.token },
    });
    const {data: {supervisors}} = await axios.get(`${Config.API_URL}/misc/supervisors`, {
      headers: { Authorization: payload.token },
    })
    const {data: {branch_heads}} = await axios.get(`${Config.API_URL}/misc/branch-heads`, {
      headers: { Authorization: payload.token },
    })

    return dispatch(setSalesProfile({ ...request.data.salesProfile, supervisors, branch_heads, onRequest: false}));
  } catch (err) {
    return dispatch(setSalesProfile({ onRequest: false }));
  }
};

const reqSalesProfile = () => {
  return {
    type: "REQ_SALES_PROFILE",
  };
};

const setSalesProfile = payload => {
  return {
    type: "SET_SALES_PROFILE",
    payload,
  };
};

export { doAuth, authLocalJWT, authLogout, getSalesProfile, setSalesProfile, onBoardState };
