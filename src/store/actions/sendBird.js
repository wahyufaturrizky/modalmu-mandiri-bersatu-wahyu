import { SendBirdAction } from "../SendBirdAction";

const sb = new SendBirdAction();

export const sbConnectSuccess = ( payload ) => ({
  type: "CONNECT_SUCCESS",
  payload
})

export const sbConnectFailed = ( error ) => ({
  type: "CONNECT_FAILED",
  error
})

export const sbDisconnectSuccess = () => ({
  type: "DISCONNECT_SUCCESS"
})

export const sbDisconnectFailed = () => ({
  type: "DISCONNECT_FAILED"
})

export const sbFetchMessagesSuccess = ( payload ) => ({
  type: "FETCH_MESSAGES_SUCCESS",
  payload
})

export const sbFetchMessagesFailed = ( error ) => ({
  type: "FETCH_MESSAGES_FAILED",
  error
})

export const sbSendMessageSuccess = ( payload ) => ({
  type: "SEND_MESSAGE_SUCCESS",
  payload
})

export const sbSendMessageFailed = ( error ) => ({
  type: "SEND_MESSAGE_FAILED",
  error
})

export const sendBirdConnect = (userId, nickname) => {
  return (dispatch) => {
    sb.connect(userId, nickname)
      .then((user) => {
        dispatch(sbConnectSuccess(user));
      })
      .catch( err => {
        dispatch(sbConnectFailed(err));
      })
  }
}

export const sendBirdDisconnect = () => {
  return (dispatch) => {
    sb.disconnect()
    .then(() => {
      dispatch(disconnectSuccess);
    })
    .catch(err => {
      dispatch(disconnectFailed);
    })
  }
}

export const sendBirdFetchMessages = (channel) => {
  return (dispatch) => {
    sb.getMessageList(channel, true)
    .then(messages => {
      dispatch(sbFetchMessagesSuccess(messages))
    })
    .catch( err => {
      dispatch(sbFetchMessagesFailed(err))
    })
  }
}

export const sendBirdSendMessage = (payload) => {
  return (dispatch) => {
    sb.sendUserMessage(payload)
    .then(message => {
      dispatch(sbSendMessageSuccess(message));
    })
    .catch( err => {
      dispatch(sbSendMessageFailed(err))
    })
  }
}